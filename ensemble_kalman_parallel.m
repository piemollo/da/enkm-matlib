%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [mu_est, mu_log, misfit] = ensemble_kalman_parallel(mu_0, L, y, 
%   solve_fwd, gamma, tol)
%
% Compute state and parameter estimations of the problem defined by 
% the functional 'solve_fwd', corresponding to observations 'y' given by
% the linear observation operator 'L'.
% Here, 'd' is the parameter dimension, 'J' is the number of particles,
% 'M' is the observation dimension and 'K' the number of time steps in 
% case of time-dependent system.
%
%  input: - mu_0: initial set of particles [d-by-J array]
%         - L: linear observation operator [M-by-Nh array]
%         - y: observations [M-by-K array]
%         - solve_fwd: functional to solve the forward problem to some 
%                      parameter value, i.e. uh = solve_fwd(mu)
%         - gamma: the noise covariance matrix
%         - tol: error tolerance to reach
%         - kmax: maximum numbder of iterations
%
% output: - mu_est: parameter estimation
%         - mu_log: list of particle evolution
%         - misfit: evolution of the observations misfit
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mu_est, mu_log, misfit] = ensemble_kalman_parallel(mu_0, L, ...
	y, solve_fwd, gamma, tol, kmax)

% ------ Initialization
[d,J] = size(mu_0);

Uh      = zeros(size(L,2),J);
mu_log  = zeros(d,kmax);
misfit= zeros(J,kmax);
mu_est  = zeros(d,1);

mu = log(mu_0);
R  = chol(gamma);
Y  = repmat(y,1,J) + R'*randn(length(y),J);

eta = tol+1;
k   = 1;

% ------ Inversion loop
while k <= kmax && eta > tol
    % --- Prediction phase
    % - Propagation
    for j = 1:J
        Uh(:,j) = solve_fwd(exp(mu(:,j)));
    end
    
    % - Update
    Gmu   = L*Uh;
    misfit(1:J,k) = sqrt(sum((Gmu - y).^2,1)');

    % - Empirical mean
    G_mean  = mean(Gmu,2);
    mu_mean = mean(mu,2);

    % - Empirical cov.
    P = -(G_mean *G_mean');
    Q = -(mu_mean*G_mean');
    for j = 1:J
      P = P + (1/J).*Gmu(:,j)*Gmu(:,j)';
      Q = Q + (1/J).*mu(:,j)*Gmu(:,j)';
    end

    % --- Analysis phase
    % - Gain [ K = M*(S^-1) ]
    mu = mu + Q*((P+gamma)\(Y-Gmu));

    % - Artificial meas
    Y = repmat(y,1,J) + R'*randn(length(y),J);

    % - Estimation
    mu_old = mu_est;
    mu_est = exp(mean(mu,2));
    mu_log(:,k) = mu_est;
    
    % --- Stop criterion
    eta = norm(mu_est-mu_old)/norm(mu_est);
    k = k+1;
end

% --- Resize
k=k-1;
mu_log  = mu_log(:,1:k);
misfit= misfit(:,1:k);
end