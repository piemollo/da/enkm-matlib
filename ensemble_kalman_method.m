%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [mu_est, mu_log, misfit] = ensemble_kalman_method(mu_0, L, y, 
%   solve_fwd, gamma, tol, kmax, phi_fwd, phi_bwd)
%
% Compute state and parameter estimations of the problem defined by 
% the functional 'solve_fwd', corresponding to observations 'y' given by
% the linear observation operator 'L'.
% Here, 'd' is the parameter dimension, 'J' is the number of particles,
% 'M' is the observation dimension.
%
%  input: - mu_0: initial set of particles       [d-by-J array]
%         - L: linear observation operator       [M-by-Nh array]
%         - y: observations                      [M vector]
%         - solve_fwd: functional to solve the   [function]
%                      forward problem to some 
%                      parameter value, i.e. 
%                        uh = solve_fwd(mu)
%         - gamma: the noise covariance matrix   [M-by-M array]
%         - tol: error tolerance to reach        [float]
%         - kmax: maximum numbder of iterations  [int]
%         - phi_fwd: parameter pre-processing    [function (optional)]
%         - phi_bwd: parameter retro-processing  [function (optional)]
%           /!/ should be such that: phi_bwd(phi_fwd(mu)) = mu
%
% output: - mu_est: parameter estimation         [d vector]
%         - mu_log: list of particle evolution   [d-by-#iterations array]
%         - misfit: evolution of the observa-    [J-by-#iterations array]
%                   tions misfit
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mu_est, mu_log, misfit] = ensemble_kalman_method(mu_0, L, ...
	y, solve_fwd, gamma, tol, kmax, phi_fwd, phi_bwd)

% ------ Parameter pre-processing
if nargin <= 8
    phi_fwd = @(x) x;
    phi_bwd = @(x) x;
end

% ------ Initialization
[d,J] = size(mu_0);

Uh      = zeros(size(L,2),J);
mu_log  = zeros(d,kmax);
misfit  = zeros(J,kmax);
mu_est  = zeros(d,1);

mu = phi_fwd(mu_0);
R  = chol(gamma);
Y  = repmat(y,1,J) + R'*randn(length(y),J);

eta = tol+1;
k   = 1;

if isa(L, 'double')
    LL = @(u) L*u;
else
    LL = @(u) L(u);
end

% ------ Inversion loop
while k <= kmax && eta > tol
    % --- Prediction phase
    % - Propagation
    for j = 1:J
        Uh(:,j) = solve_fwd( phi_bwd( mu(:,j) ) );
    end
    
    % - Update
    Gmu   = LL(Uh);
    misfit(:,k) = sqrt(sum((Gmu - y).^2,1)');

    % - Empirical mean
    G_mean  = mean(Gmu,2);
    mu_mean = mean(mu,2);

    % - Empirical cov.
    P = -( G_mean*G_mean');
    Q = -(mu_mean*G_mean');
    for j = 1:J
      P = P + (1/J).*Gmu(:,j)*Gmu(:,j)';
      Q = Q + (1/J).* mu(:,j)*Gmu(:,j)';
    end

    % --- Analysis phase
    % - Gain [ K = M*(S^-1) ]
    mu = mu + Q*((P+gamma)\(Y-Gmu));

    % - Artificial meas
    Y = repmat(y,1,J) + R'*randn(length(y),J);

    % - Estimation
    mu_old = mu_est;
    mu_est = phi_bwd( mean(mu,2) );
    mu_log(:,k) = mu_est;
    
    % --- Stop criterion
    eta = norm(mu_est-mu_old)/norm(mu_est);
    k = k+1;
end

% --- Resize
k = k-1;
mu_log  = mu_log(:,1:k);
misfit  = misfit(:,1:k);
end