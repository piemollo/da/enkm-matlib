%% ====== Validation Ensemble Kalman Method and parallel version
rng(18);

% ------ Problem statement

% --- Parameter: mu \in (0,1)^d
d      = 3;
mu_set = [ones(1,d).*0; ones(1,d).*1];

% --- Forward problem: u(mu)
Nx = 500;
x  = linspace(0,5,Nx)';

u  = @(x,mu) ...
    2.*mu(1).*exp(-0.5.*(x-1).^2) + ...
    2.*mu(2).*exp(-0.5.*(x-3).^2) + ...
    sin(0.5.*pi.*(x-mu(3))) ...
    ;

% --- Linear observation operator
M   = 10;
Idm = floor(linspace(Nx*0.05,Nx*0.95,M));
LM  = zeros(M,Nx);
for i = 1:M
    LM(i,Idm(i)) = 1;
end

% --- Display problem
Ns        = 10;
mu_sample = rand(Ns,d) .* (mu_set(2,:)-mu_set(1,:)) + mu_set(1,:);

figure(1)
clf;
set(gcf, 'Renderer','painters');
hold on
grid on

% Variability
u_bnd = [0,0];
for i = 1:Ns
    U = u(x, mu_sample(i,:));
    plot(x, U, 'color', [0 0.4470 0.7410])
    u_bnd(1) = min(u_bnd(1), min(U) );
    u_bnd(2) = max(u_bnd(2), max(U) );

    LMU = LM*U;
    plot(x(Idm), LMU, 'or', 'MarkerSize', 3, 'LineWidth', 1);
end

% Measures
for i = 1:M
    plot([x(Idm(i)), x(Idm(i))], [u_bnd(1),u_bnd(2)], ':r');
end

title('Parameterized model', 'interpreter', 'latex')
xlabel('x', 'interpreter', 'latex')
ylabel('u(x,.)', 'interpreter', 'latex')
axis([0, 5, u_bnd(1), u_bnd(2)])
set(gca, 'FontSize', 12)
saveas(gcf,'./assets/param_model.png');

%% ------ Data assimilation setup

% True target
mu_true = rand(1,d) .* mu_set(2,:)-mu_set(1,:) + mu_set(1,:);
U_true  = u(x,mu_true);
y_true  = LM*U_true; 

% Setup 
fwd_pb    = @(mu) u(x,mu); % Forward function
Gamma     = eye(M).*1e-1;  % Artificial noise
tolerance = 1e-3;          % Estimation tolerance
kmax      = 50;            % Maximum number of iteration

% Display results
figure(2)
clf;

% --- 10 Particles
% Particles
Nj      = 10; 
mu_init = rand(Nj,d) .* mu_set(2,:)-mu_set(1,:) + mu_set(1,:);
mu_init = mu_init';

[mu_est, mu_log, misfit_10] = ensemble_kalman_method(...
    mu_init, ...
    LM, ...
	y_true, ...
    fwd_pb, ...
    Gamma, ...
    tolerance, ...
    kmax...
    );

% display results
subplot(3,1,1)
set(gcf, 'Renderer','painters');
hold on
grid on

plot(x, U_true, 'color', [0.8500 0.3250 0.0980], 'LineWidth', 1.2)

for j = 1:size(mu_log,2)
    U = u(x, mu_log(:,j));
    plot(x, U, ':', 'color', [0.9 0.6 0.2], 'LineWidth', 0.2)
end

title(sprintf('%d particles: Estimated in %d iterations', ...
    Nj, size(mu_log,2)), 'interpreter', 'latex')
xlabel('x', 'interpreter', 'latex')
ylabel('u(x,.)', 'interpreter', 'latex')
axis([0, 5, u_bnd(1), u_bnd(2)])
set(gca, 'FontSize', 9)

% --- 20 Particles
% Particles
Nj      = 20; 
mu_init = rand(Nj,d) .* mu_set(2,:)-mu_set(1,:) + mu_set(1,:);
mu_init = mu_init';

[mu_est, mu_log, misfit_20] = ensemble_kalman_method(...
    mu_init, ...
    LM, ...
	y_true, ...
    fwd_pb, ...
    Gamma, ...
    tolerance, ...
    kmax...
    );

% display results
subplot(3,1,2)
set(gcf, 'Renderer','painters');
hold on
grid on

plot(x, U_true, 'color', [0.8500 0.3250 0.0980], 'LineWidth', 1.2)

for j = 1:size(mu_log,2)
    U = u(x, mu_log(:,j));
    plot(x, U, ':', 'color', [0.9 0.6 0.2], 'LineWidth', 0.2)
end

title(sprintf('%d particles: Estimated in %d iterations', ...
    Nj, size(mu_log,2)), 'interpreter', 'latex')
xlabel('x', 'interpreter', 'latex')
ylabel('u(x,.)', 'interpreter', 'latex')
axis([0, 5, u_bnd(1), u_bnd(2)])
set(gca, 'FontSize', 9)

% --- 20 Particles [pre-processed]
% Parameter processing
phi_f = @(mu) log(mu);
phi_b = @(mu) exp(mu);

% Particles
Nj      = 20; 
mu_init = rand(Nj,d) .* mu_set(2,:)-mu_set(1,:) + mu_set(1,:);
mu_init = mu_init';

[mu_est, mu_log, misfit_pr] = ensemble_kalman_method(...
    mu_init, ...
    LM, ...
	y_true, ...
    fwd_pb, ...
    Gamma, ...
    tolerance, ...
    kmax,...
    phi_f, ...
    phi_b ...
    );

% display results
subplot(3,1,3)
set(gcf, 'Renderer','painters');
hold on
grid on

plot(x, U_true, 'color', [0.8500 0.3250 0.0980], 'LineWidth', 1.2)

for j = 1:size(mu_log,2)
    U = u(x, mu_log(:,j));
    plot(x, U, ':', 'color', [0.9 0.6 0.2], 'LineWidth', 0.2)
end

title(sprintf(['%d particles: Estimated in %d iterations ' ...
    '(pre-processed)'], ...
    Nj, size(mu_log,2)), 'interpreter', 'latex')
xlabel('x', 'interpreter', 'latex')
ylabel('u(x,.)', 'interpreter', 'latex')
axis([0, 5, u_bnd(1), u_bnd(2)])
set(gca, 'FontSize', 9)
saveas(gcf,'./assets/approx.png');

%% ------ Display misfit
figure(3)
clf;
set(gcf, 'Renderer','painters');

semilogy(1:size(misfit_10,2), mean(abs(misfit_10),1), '*')
hold on
grid on
semilogy(1:size(misfit_20,2), mean(abs(misfit_20),1), 'p')

title('Misfit evolution', 'interpreter', 'latex')
xlabel('Iterations', 'interpreter', 'latex')
ylabel('$\|y - L_M(u(\mu^*))\|$', 'interpreter', 'latex')
legend('10 particles', '20 particles', 'FontSize', 12, 'interpreter', 'latex')
set(gca, 'FontSize', 12)
saveas(gcf,'./assets/misfit.png');