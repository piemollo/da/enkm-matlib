# Ensemble Kalman Method for Matlab(c) / GNU Octave

This module provides Ensemble Kalman Method (EnKM) function to 
solve inverse problems.
The module also provides a validation example to show standard 
usages and implemented features.
## How to use it

To use this module, one needs to first download the repository 
using the command
````bash
git clone https://gitlab.com/piemollo/da/enkm/enkm-matlib
````
and then add the path to the repository into matlab(c) / GNU octave
using the command
````Matlab
addpath <path-to>/enkm-matlib
````
One have now access to module functions.

<details><summary>List of function(s)</summary>

| Name | Description |
| --- | --- |
| [`ensemble_kalman_method`](#enkm)    | Standard EnKM function                |
| [`ensemble_kalman_parallel`](#enkmp) | EnKM with parallel pool for particles |
| [`ensemble_kalman_dynamic`](#enkmd)  | EnKM for time dependent problems      |
|                                      |                                       |
| [`validation`](#val)                 | Validation example                    |
</details>

## Module description

<a name=enkm></a>

### **`ensemble_kalman_method.m`**

#### Description
This function solves the parameter identification problem associated to 
forward problem and observation operator prescribed.
Here:
* $d$ is the dimension of the parameter space, 
* $J$ is the number of particles, 
* $M$ is the dimension of the observation space, 
* $N_h$ is the dimension of the discrete approximation space 
for the model solution.

#### Note
* one can prescribe pre-processing functions on the parameter
to navigate between the true parameter space and more appropriate computational 
space.
These two functions: forward map `phi_fwd` and backward map `phi_bwd` should 
be such that `phi_fwd(phi_bwd(mu)) == mu`.
By default they are set to identity operator.
* The observation operator `L` can be prescribed as a matrix in the linear case
or as an anonymous function to handle possibly non-linear cases.


#### Syntax

```Matlab
mu_estimate = ensemble_kalman_method(mu_0, L, y, solve_fwd, gamma, tol);
% or
[mu_est, mu_log, misfit] = ensemble_kalman_method(mu_0, L, y, solve_fwd, ...
    gamma, tol, phi_fwd, phi_bwd)
```

#### Arguments

| Input       | Format        | Description                            |
| -----       | ------        | -----------                            |
| `mu_0`      | `array(d,J)`  | Initial particle pool                  |
| `L`         | `array(M,Nh)` | Linear observation operator            |
|             | `@func`       | Functional observation operator        |
| `y`         | `vect(M)`     | Measurements to fit                    |
| `solve_fwd` | `@func`       | Forward problem solver                 |
| `gamma`     | `array(M,M)`  | Artificial noise covariance mat.       |
| `tol`       | `float`       | Targeted tolerance                     |
| `kmax`      | `float`       | Iteration limit                        |
| `phi_fwd`   | `@func`       | (opt.) Forward preprocessing function  |
| `phi_bwd`   | `@func`       | (opt.) Backward preprocessing function |

| Output      | Format           | Description                            |
| -----       | ------           | -----------                            |
| `mu_est`    | `vect(d)`        | Estimated parameter                    |
| `mu_log`    | `array(d,#iter)` | Estimated parameter at each iteration  |
| `misfit`    | `array(M,#iter)` | Misfit between estimated obs. and `y`  |



<a name=enkmp></a>

### **`ensemble_kalman_parallel.m`**

tbd.

<a name=enkmd></a>

### **`ensemble_kalman_method.m`**

tbd.

<a name=val></a>

### **`validation.m`**

This script provides a simple example to validate functions presented here 
and to show how to use them.
In this example, the following parameterized function is used
$$
u(x, [\mu_1, \mu_2, \mu_3]) = 
2\mu_1 \exp(-0.5(x-1)^2) + 
2\mu_2 \exp(-0.5(x-3)^2) + 
\sin( 0.5\pi(x-\mu_3) )
$$
on the interval $[0,5]$.
Observation operator corresponds to pointwise evaluation at equi-distributed 
positions.
A sample of $u$ functions for different parameter values is showed in the 
following figure, red dash line corresponds to evaluation positions and 
red circles to functions measurements.

![](assets/param_model.png)

To validate the function contained in the module, a specific parameter 
is selected, denoted `mu_true`, and corresponding  measurements are 
computed, denoted `y`.
Then an estimation of `mu_true` is computed using the function 
`ensemble_kalman_mathod` in different settings:
* with 10 particles,
* with 20 particles,
* with 20 particles and parameter preprocessing; `phi_fwd=log`, `phi_bwd=exp`.
The tolerance is set to $10^{-3}$ and results are showed in the following 
figure.

![](assets/approx.png)

The misfit between estimated observations and measurements are showed in the 
following figure.

![](assets/misfit.png)